/**
 * Indexing end point                                                            (36) V
 * 1.  User      -> login, register, update, detail                              (4)  V
 * 2.  Category  -> add  , update  , delete, get   , detail                      (5)  V
 * 3.  Brand     -> add  , update  , get   , nama                                (4)  V
 * 4.  Unit      -> add  , update  , get   , detail, conversion to base          (5)  V 
 * 5.  Location  -> add  , update  , delete, get   , detail            , nearby  (6)  V 
 * 6.  Supplier  -> add  , update  , get   , detail                              (4)  V
 * 7.  Item      -> add  , update  , delete, get   , detail                      (5)  V
 * 8.  Transaksi -> add  , get     , detail                                      (3)  V
 */

const express    = require('express');
const bodyparser = require('body-parser');
const config     = require('./config');
const app        = express();
const user       = require('./user');
const category   = require('./category');
const brand      = require('./brand');
const unit       = require('./unit');
const location   = require('./location');
const supplier   = require('./supplier'); 
const items      = require('./items');
const trans      = require('./trans');

app.use    (bodyparser.json());
app.use    ('/api/users'    , user);
app.use    ('/api/category' , category);
app.use    ('/api/brand'    , brand);
app.use    ('/api/unit'     , unit);
app.use    ('/api/location' , location);
app.use    ('/api/supplier' , supplier);
app.use    ('/api/items'    , items);
app.use    ('/api/trans'    , trans);
app.listen (config.PORT || process.env.PORT || 3000);
console.log('Server started at ', config.PORT || process.env.PORT || 3000);