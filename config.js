module.exports = {
    "mysql": {
        connectionLimit : 100,
        host     : 'localhost',
        user     : 'root',
        password : '',
        database : 'db_soa',
        debug    :  false
    },
    "port": 3000,
    "secret" : "proyek-soa-secret",
    "refreshTokenSecret" : "proyek-soa-secret-token",
    "tokenLife" : 900,
    "refreshTokenLife" : 86400
}