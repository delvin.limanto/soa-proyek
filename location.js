const express    = require('express');
const router     = express.Router();
const mysql      = require('mysql');
const config     = require('./config');
const jwtChecker = require('./tokenChecker');
const keyChecker = require('./keyChecker');

router.use(keyChecker);
router.use(jwtChecker);
/**
 * API untuk menambahkan data lokasi
 * @link    localhost:3000/api/location/add
 * @method  POST
 * 
 * @return  JSON
 */
router.post('/add', (req,res) => {
    let postData = req.body;
    let pool     = mysql.createPool(config.mysql);
    let query    = "INSERT INTO location(nama_location,alamat_location,kodepos_location,lat_location,long_location,status_location) VALUES(?,?,?,?,?,?)";
    let table    = [ postData.nama, postData.alamat, postData.kodepos, postData.lat, postData.long, "1" ];
    query        = mysql.format(query, table);
    pool.query(query, (err, rows) => {
        if(err){
            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Terjadi kesalahan" });
        }else{
            res.status(200).json({ "code" : "200", "status"  : "Success", "message" : "Berhasil menambahkan lokasi" });
        }
    });
});

/**
 * API untuk mengubah data lokasi
 * @link    localhost:3000/api/location/update
 * @method  PUT
 * 
 * @return  JSON
 */
router.put('/update', (req,res) => {
    let postData = req.body;
    let pool     = mysql.createPool(config.mysql);
    let query    = "UPDATE location SET nama_location = ?, alamat_location = ?, kodepos_location = ?, lat_location = ?, long_location = ?, status_location = ? WHERE id_location = ?";
    let table    = [ postData.nama, postData.alamat, postData.kodepos, postData.lat, postData.long, postData.status, postData.id ];
    query        = mysql.format(query, table);
    pool.query(query, (err, rows) => {
        if(err){
            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Email telah dipakai" });
        }else{
            res.status(200).json({ "code" : "200", "status"  : "Success", "message" : "Berhasil mengubah lokasi" });
        }
    });
});

/**
 * API untuk menghapus data lokasi
 * @link    localhost:3000/api/location/delete/:id
 * @method  PUT
 * 
 * @return  JSON
 */
router.put('/delete/:id', (req,res) => {
    let id       = req.params.id;
    let pool     = mysql.createPool(config.mysql);
    let query    = "UPDATE location SET status_location = '0' WHERE id_location = ?";
    let table    = [ id ];
    query        = mysql.format(query, table);
    pool.query(query, (err, rows) => {
        if(err){
            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Terjadi kesalahan" });
        }else{
            res.status(200).json({ "code" : "200", "status"  : "Success", "message" : "Berhasil menghapus lokasi" });
        }
    });
});

/**
 * API untuk mendapatkan seluruh data lokasi
 * @link    localhost:3000/api/location/get?limit=:limit&offset=:offset
 * @method  GET
 * 
 * @return  JSON
 */
router.get('/get', (req,res) => {
    let pool   = mysql.createPool(config.mysql);
    let limit  = req.query.limit;
    let offset = req.query.offset;
    let query  = "SELECT * FROM location LIMIT ?, ?";
    if(limit  == null || offset == null){
        query  = "SELECT * FROM location";
    }else{
        query  = mysql.format(query, [ parseInt(offset), parseInt(limit) ]);
    }
    pool.query(query, (err, rows) => {
        if(err){
            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Terjadi kesalahan" });
        }else{
            res.status(200).json({ "code" : "200", "status"  : "Success", "value" : rows });
        }
    });
});

/**
 * API untuk mendapatkan detail data lokasi
 * @link    localhost:3000/api/location/detail/:id
 * @method  GET
 * 
 * @return  JSON
 */
router.get('/detail/:id', (req,res) => {
    let id    = req.params.id;
    let pool  = mysql.createPool(config.mysql);
    let query = "SELECT * FROM location WHERE id_location = ?";
    query     = mysql.format(query, [ id ]);
    pool.query(query, (err, rows) => {
        if(err){
            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Terjadi kesalahan" });
        }else{
            res.status(200).json({ "code" : "200", "status"  : "Success", "value" : rows });
        }
    });
});

/**
 * API untuk mendapatkan detail data unit ukur
 * @link    localhost:3000/api/location/nearby/:radius?lat=:lat&long=:long&limit=:limit&offset=:offset
 * @method  GET
 * 
 * @return  JSON
 */
router.get('/nearby/:radius', (req,res) => {
    let lat   = req.query.lat;
    let long  = req.query.long;
    if(lat == null || long == null){
        res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Posisi saat ini tidak boleh null" });
    }else{
        let rad   = req.params.radius;
        let limit  = req.query.limit;
        let offset = req.query.offset;
        let pool  = mysql.createPool(config.mysql);
        let query = "SELECT *, dist(lat_location, long_location, ?, ?) as distance FROM location WHERE distance <= ? LIMIT ?,?";
        if(limit == null || offset == null){
            query = "SELECT *, dist(lat_location, long_location, ?, ?) as distance FROM location WHERE distance <= ?";
            query = mysql.format(query, [ lat, long, rad ]);    
        }else{
            query = mysql.format(query, [ lat, long, rad, limit, offset ]);
        }
        pool.query(query, (err, rows) => {
            if(err){
                res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Terjadi kesalahan" });
            }else{
                res.status(200).json({ "code" : "200", "status"  : "Success", "value" : rows });
            }
        });
    }
});

module.exports = router;