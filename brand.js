const express    = require('express');
const router     = express.Router();
const mysql      = require('mysql');
const config     = require('./config');
const jwtChecker = require('./tokenChecker');
const keyChecker = require('./keyChecker');

router.use(keyChecker);
router.use(jwtChecker);
/**
 * API untuk menambahkan brand
 * @link    localhost:3000/api/category/add
 * @method  POST
 * 
 * @return  JSON
 */
router.post('/add', (req,res) => {
    let postData = req.body;
    let pool     = mysql.createPool(config.mysql);
    let query    = "INSERT INTO brand(nama_brand) VALUES(?)";
    let table    = [ postData.nama ];
    query        = mysql.format(query, table);
    pool.query(query, (err, rows) => {
        if(err){
            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Terjadi kesalahan" });
        }else{
            res.status(200).json({ "code" : "200", "status"  : "Success", "message" : "Berhasil menambahkan brand" });
        }
    });
});

/**
 * API untuk mengubuah nama brand
 * @link    localhost:3000/api/brand/update
 * @method  PUT
 * 
 * @return  JSON
 */
router.put('/update', (req,res) => {
    let postData = req.body;
    let pool     = mysql.createPool(config.mysql);
    let query    = "UPDATE brand SET nama_brand = ? WHERE id_brand = ?";
    let table    = [ postData.nama, postData.id ];
    query        = mysql.format(query, table);
    pool.query(query, (err, rows) => {
        if(err){
            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Email telah dipakai" });
        }else{
            res.status(200).json({ "code" : "200", "status"  : "Success", "message" : "Berhasil mengubah brand" });
        }
    });
});

/**
 * API untuk mendapatkan seluruh data brand
 * @link    localhost:3000/api/brand/get
 * @method  GET
 * 
 * @return  JSON
 */
router.get('/get', (req,res) => {
    let pool   = mysql.createPool(config.mysql);
    let limit  = req.query.limit;
    let offset = req.query.offset;
    let query  = "SELECT id_brand, nama_brand FROM brand LIMIT ?, ?";
    if(limit  == null || offset == null){
        query  = "SELECT id_brand, nama_brand FROM brand";
    }else{
        query  = mysql.format(query, [ parseInt(offset), parseInt(limit) ]);
    }
    pool.query(query, (err, rows) => {
        if(err){
            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Terjadi kesalahan" });
        }else{
            res.status(200).json({ "code" : "200", "status"  : "Success", "value" : rows });
        }
    });
});

/**
 * API untuk mendapatkan nama brand
 * @link    localhost:3000/api/brand/get/nama/:id
 * @method  GET
 * 
 * @return  JSON
 */
router.get('/get/nama/:id', (req,res) => {
    let id     = req.params.id;
    let pool   = mysql.createPool(config.mysql);
    let query  = "SELECT nama_brand FROM brand WHERE id_brand";
    query  = mysql.format(query, [ id ]);
    pool.query(query, (err, rows) => {
        if(err){
            res.status(400).json({ "code" : "400", "status"  : "Error", "message" : "Terjadi kesalahan" });
        }else{
            res.status(200).json({ "code" : "200", "status"  : "Success", "name" : rows[0].nama_brand });
        }
    });
});

module.exports = router;